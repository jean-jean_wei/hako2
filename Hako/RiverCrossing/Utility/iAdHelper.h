//
//  iAdHelper.h
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-25.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iAd/iAd.h>

@interface iAdHelper : NSObject <ADBannerViewDelegate>
{
    BOOL iAdVisible;
    ADBannerView *adView;
}

+ (iAdHelper*)instance;
- (void) setupiAd;

@end
